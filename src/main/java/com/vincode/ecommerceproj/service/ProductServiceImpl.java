package com.vincode.ecommerceproj.service;

import com.vincode.ecommerceproj.model.Product;
import com.vincode.ecommerceproj.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void addItems(Product product) {
        productRepository.save(product);
    }

    @Override
    public Iterable<Product> getAllItems() {
        return productRepository.findAll();
    }

    @Override
    public void updateItem(Long id, Product product) {
        Product productToUpdate = productRepository.findById(id).get();
        productToUpdate.setProductName(product.getProductName());
        productToUpdate.setProductType(product.getProductType());
        productToUpdate.setDescription(product.getDescription());
        productToUpdate.setPrice(product.getPrice());
        productToUpdate.setStocks(product.getStocks());
        productToUpdate.setImageLink(product.getImageLink());
        productRepository.save(productToUpdate);
    }

    @Override
    public void deleteItem(Long id) {
        productRepository.deleteById(id);
    }
}
