package com.vincode.ecommerceproj.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String productName;

    @Column(name = "category")
    private String productType;

    @Lob
    @Column(name = "description", length = 1000)
    private String description;

    @Column(name = "price")
    private double price;

    @Column(name = "stocks")
    private int stocks;

    @Column(name = "image")
    private String imageLink;
}
