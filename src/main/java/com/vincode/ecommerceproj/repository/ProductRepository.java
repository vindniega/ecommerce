package com.vincode.ecommerceproj.repository;

import com.vincode.ecommerceproj.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {

}
