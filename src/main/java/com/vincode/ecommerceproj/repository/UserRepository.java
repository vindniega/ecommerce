package com.vincode.ecommerceproj.repository;

import com.vincode.ecommerceproj.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
